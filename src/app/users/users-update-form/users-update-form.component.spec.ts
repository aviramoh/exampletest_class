import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersUpdateFormComponent } from './users-update-form.component';

describe('UsersUpdateFormComponent', () => {
  let component: UsersUpdateFormComponent;
  let fixture: ComponentFixture<UsersUpdateFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersUpdateFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersUpdateFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
