import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import { AngularFireDatabase } from 'angularfire2/database';
import 'rxjs/Rx';

@Injectable()
export class UsersService {
  http:Http;

  /*getUsers() {
    let token = localStorage.getItem('token');
    return this.http.get(environment.url +'users/'+token);
  }*/
  getUsers()
  {
    let token='?token='+localStorage.getItem('token');
    return this.http.get('http://localhost/angular/slim/users'+token);
  }

  getMessagesFire(){
    return this.db.list('/users').valueChanges();
  }

  /*postUser(data){
    let token = localStorage.getItem('token');
    let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name', data.name).append('phone_number', data.phone_number);
    return this.http.post(environment.url +'users/'+token, params.toString(), options);
    
  }*/

  postUser(data)
  {
    let token = localStorage.getItem('token');
    let params = new HttpParams().append('name',data.name).append('token',token);      
    let options =  {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'    
      })
    }      
    return this.http.post('http://localhost/angular/slim/users', params.toString(), options);
  }

  /*deleteMessage(key){

    return this.http.delete(environment.url +'users/'+key);
  }*/

   deleteMessage(key)
  {
    let token=localStorage.getItem('token');
    console.log(key);  
    console.log(token);
    return this.http.delete('http://localhost/angular/slim/users/:'+ key + '/:' + token);
  }

  getUser(id) {
    return this.http.get(environment.url +'users/' + id);
  }

  putUser(data,key){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phone_number',data.phone_number);
    return this.http.put(environment.url +'users/'+ key,params.toString(), options);
  }

  login(credentials){
        let options = {
      headers:new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('user', credentials.user).append('password', credentials.password);
    return this.http.post(environment.url + 'auth', params.toString(), options).map(response=>{
      let token = response.json().token; //the JWT that comes from the DB
      if(token) localStorage.setItem('token', token);
      console.log(token);
    });
  }

  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http
   }

}
