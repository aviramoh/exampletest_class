import { UsersService } from './users.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users;
  usersKeys = [];

  constructor(private service:UsersService) {
    service.getUsers().subscribe(response => {
      this.users = response.json();
      this.usersKeys = Object.keys(this.users);
      });
   }

   optimisticAdd(user){
    //console.log("addUser worked" + name);
    var newKey = this.usersKeys[this.usersKeys.length-1] + 1;
    var newMessageObject = {};
    newMessageObject['name'] = name;
    this.users[newKey] = newMessageObject;
    this.usersKeys = Object.keys(this.users);
  }

  pessimisticAdd(){
    this.service.getUsers().subscribe(response => {
    //console.log(response.json());
    this.users = response.json();
    this.usersKeys = Object.keys(this.users);
  })}

  deleteMessage(key){
    console.log(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1);
    //delete from server
    this.service.deleteMessage(key).subscribe(
      response=>console.log(response)
    );
  }

  ngOnInit() {
  }

}
