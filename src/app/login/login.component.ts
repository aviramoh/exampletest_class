import { Router } from '@angular/router';
import { UsersService } from './../users/users.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  invalid = false;
  loginform = new FormGroup({
    user:new FormControl(),
    password:new FormControl(),
  });

  login(){
    this.service.login(this.loginform.value).subscribe(response=>{
      this.router.navigate(['/']);
    }, error=>{
      this.invalid = true;
    })
  }

  logout(){
    localStorage.removeItem('token');
    this.invalid = false;
  }

  constructor(private service:UsersService, private router:Router) { }

  ngOnInit() {
  }

}
