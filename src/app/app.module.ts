import { environment } from './../environments/environment';
import { HttpModule } from '@angular/http';
import { UsersService } from './users/users.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule} from '@angular/router';
import { FormsModule, ReactiveFormsModule} from "@angular/forms";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';


import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersFormComponent } from './users/users-form/users-form.component';
import { UsersUpdateFormComponent } from './users/users-update-form/users-update-form.component';
import { UsersFirebaseComponent } from './users-firebase/users-firebase.component';
import { LoginComponent } from './login/login.component';


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    UsersFormComponent,
    UsersUpdateFormComponent,
    UsersFirebaseComponent,
    LoginComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {pathMatch:'full', path:'', component:UsersComponent},
      {pathMatch:'full', path:'products', component:ProductsComponent},
      {pathMatch:'full', path:'users-update-form/:id', component:UsersUpdateFormComponent},
      {pathMatch:'full', path:'users-firebase', component:UsersFirebaseComponent},
      {pathMatch:'full', path:'login', component:LoginComponent},
      {pathMatch:'full', path:'**', component:NotFoundComponent}//have to be last
    ])
  ],
  providers: [
    UsersService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
